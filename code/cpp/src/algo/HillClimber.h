#include "search.h"
#include "MixedQAPEval.h"
#include "NeighborhoodMixedQAPEval.h"
#include <vector>
#include <iostream>

class HillClimber : public Search {
	public:
		HillClimber(MixedQAPEval & _eval, NeighborhoodMixedQAPEval & _neighborhoodMixedQAPEval, unsigned _n): eval(_eval), neighborhoodMixedQAPEval(_neighborhoodMixedQAPEval), n(_n) {
			delta.resize(n);

			for (unsigned int i=0; i<n; i++) {
				delta[i].resize(n);
			}
		}

		virtual void operator() (Solution & _solution) {
			bool localOpt = false;
			std::pair<unsigned int, unsigned int> move;
			int cpt=0;

			neighborhoodMixedQAPEval.init(_solution, delta);

			while(!localOpt && time(NULL) < timeLimit_) {
				selectBestNeighbor(move);
				cpt++;
				if (delta[move.first][move.second] < 0) {
					unsigned tmp = _solution.sigma[move.first];
					_solution.sigma[move.first] = _solution.sigma[move.second];
					_solution.sigma[move.second] = tmp;
					_solution.fitness += delta[move.first][move.second];
					neighborhoodMixedQAPEval.update(_solution, move, delta);
				}
				else {
					localOpt = true;
				}
			}

			std::cout<<"Count = "<<cpt<<std::endl;
		}

	protected:
		virtual void selectBestNeighbor(std::pair<unsigned int, unsigned int> & best) {
			unsigned int i, j;

			best.first = 1;
			best.second = 0;
			double bestDelta = delta[best.first][best.second];

			for (i=1; i<n; i++) {
				for (j=0; j<i; j++) {
					if (delta[i][j] < bestDelta) {
						best.first = i;
						best.second = j;
						bestDelta = delta[i][j];	
					}
				}
			}
		}

		MixedQAPEval & eval;
		NeighborhoodMixedQAPEval & neighborhoodMixedQAPEval;
		unsigned n;
		std::vector<std::vector<double>> delta;
};