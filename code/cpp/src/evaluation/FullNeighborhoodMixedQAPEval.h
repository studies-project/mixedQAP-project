#ifndef _FullNeighborhoodMixedQAPEval_H
#define _FullNeighborhoodMixedQAPEval_H

#include <vector>
#include "solution.h"
#include "MixedQAPEval.h"

class FullNeighborhoodMixedQAPEval : public NeighborhoodMixedQAPEval {
	public:
		FullNeighborhoodMixedQAPEval(MixedQAPEval & _eval) : eval(_eval) { }
		
		virtual void init(Solution & _solution, std::vector<std::vector<double>> & delta) {
			unsigned int tmp;
			unsigned int i, j;

			double currentFitness = _solution.fitness;

			for(i=1; i<_solution.sigma.size(); i++) {
				for(j=0; j<i; j++) {
					tmp = _solution.sigma[i];
					_solution.sigma[i] = _solution.sigma[j];
					_solution.sigma[j] = tmp;

					eval(_solution);
					delta[i][j] = _solution.fitness - currentFitness;

					_solution.sigma[j] = _solution.sigma[i];
					_solution.sigma[i] = tmp;
				}
			}
			_solution.fitness = currentFitness;
		}

		virtual void update(Solution & _solution, std::pair<unsigned int, unsigned int> & neighbor, std::vector<std::vector<double>> & delta) {
			init(_solution, delta);
		}

	protected:
		MixedQAPEval & eval;
};

#endif