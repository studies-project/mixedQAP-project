#ifndef _IncrementalMixedQAPEval_H
#define _IncrementalMixedQAPEval_H

#include <vector>
#include "solution.h"
#include "MixedQAPEval.h"

class IncrementalMixedQAPEval : public MixedQAPEval {
	public:
		IncrementalMixedQAPEval(MixedQAPEval & _eval) : eval(_eval) { }
		
		virtual void init(Solution & _solution, std::vector<std::vector<double>> & delta) {
			unsigned int tmp;
			unsigned int i, j;

			double currentFitness = _solution.fitness;

			for(i=1; i<_solution.sigma.size(); i++) {
				for(j=0; j<i; j++) {
					_solution.fitness = currentFitness;
					evaluation(_solution, i, j);
					delta[i][j] = _solution.fitness - currentFitness;
				}
			}
			_solution.fitness = currentFitness;
		}

		virtual void update(Solution & _solution, std::pair<unsigned int, unsigned int> & neighbor, std::vector<std::vector<double>> & delta) {
			init(_solution, delta);
		}

		void evaluation(Solution & solution, int i, int j) {
			int u = i, v = j;

			if (i > j) {
				u = j;
				v = i;
			}

			double sumResult= 0;
			
			// We have to avoid u and v.
			for (int k=0; k<u; k++)
				sumResult += (
					(solution.flow[u][k] - solution.flow[v][k]) *
					(eval.distance[solution.sigma[v]][solution.sigma[k]] - eval.distance[solution.sigma[u]][solution.sigma[k]])
				);

			// We how to avoid u
			for (int k=u+1; k<v; k++)
				sumResult += (
					(solution.flow[k][u] - solution.flow[v][k]) *
					(eval.distance[solution.sigma[v]][solution.sigma[k]] - eval.distance[solution.sigma[u]][solution.sigma[k]])
				);

			// We how to avoid v
			for (int k=v+1; k<eval.n; k++)
				sumResult += (
					(solution.flow[k][u] - solution.flow[k][v]) *
					(eval.distance[solution.sigma[v]][solution.sigma[k]] - eval.distance[solution.sigma[u]][solution.sigma[k]])
				);
			
			solution.fitness += 2*sumResult;

		}

	protected:
		MixedQAPEval & eval;
};

#endif