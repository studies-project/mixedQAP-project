#ifndef _NEIGHBORHOODMIXEDQAPEVAL_H
#define _NEIGHBORHOODMIXEDQAPEVAL_H

#include <vector>
#include "solution.h"

class NeighborhoodMixedQAPEval {
	public:
		virtual void init(Solution & _solution, std::vector<std::vector<double>> & delta) = 0;

		virtual void update(Solution & _solution, std::pair<unsigned int, unsigned int> & neighbor, std::vector<std::vector<double>> & delta) = 0;
};

#endif