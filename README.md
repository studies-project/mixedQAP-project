# MixedQAP

## Authors: 
- Sébastien Verel
- Ernesto Artigas
- Alexandre Fovet


## Description:

Please see the project description at: 
https://www.overleaf.com/read/phjcbfrmfycr


## Instance files:

The directory instances/ contains the set of mixed-variable Quadratic Assignment Problem (mixed-QAP) instance files.


## Code:

The directory code/src/cpp contains the c++ code. The evaluation function is in the directory code/cpp/src/evaluation, and basic tests are in the directory code/cpp/src/tests.

The directory code/src/java contains the java code. 


## To compile the code:

```sh
cd code/cpp
mkdir build
cd build
cmake ../src/tests
make
```


## To execute the tests:

```sh
./randomSearch instanceFile seed numberOfSeconds
./hillClimberExe instanceFile seed numberOfSeconds
./ilsExe instanceFile seed numberOfSeconds
./esExe instanceFile seed numberOfSeconds
```

## To process all the necessary data:

```sh
cd data
# After simplifying the output of each program to only print count, fitness for a simplier bash script.
./captureResult.sh
```
This bash script will create four csv file and can send it to an email address with the optionnal last line. A python script was written to process all four csv files, associate with each instances of mixedQAP their average count and fitness per algorithm and will average all these values to rank the algorithm.
```sh
conda create -y -n dataProcessing
conda activate dataProcessing
conda install -y numpy pandas
python3 calculationData.py
```
Output :
```
{'mixedQAP_uni_100_-100_100_1.dat': [{'algoType': 'RandomSearch',
									  'countAverage': 130102.37,
									  'fitnessAverage': 18416854.39},
									 {'algoType': 'HillClimber',
									  'countAverage': 27.63,
									  'fitnessAverage': 16912821.89},
									 {'algoType': 'ILS',
									  'countAverage': 64687.5,
									  'fitnessAverage': 19100590.7},
									 {'algoType': 'ES',
									  'countAverage': 23996.83,
									  'fitnessAverage': 19181722.32}],
...
'mixedQAP_uni_99_1_100_1.dat': [{'algoType': 'RandomSearch',
								  'countAverage': 139798.67,
								  'fitnessAverage': 36843259.47},
								 {'algoType': 'HillClimber',
								  'countAverage': 28.7,
								  'fitnessAverage': 35618603.06},
								 {'algoType': 'ILS',
								  'countAverage': 68168.27,
								  'fitnessAverage': 37446432.39},
								 {'algoType': 'ES',
								  'countAverage': 17734.7,
								  'fitnessAverage': 37288466.92}]}

Count Average
[('HillClimber', 47.86),
 ('ES', 13591.86),
 ('ILS', 379880.31),
 ('RandomSearch', 892678.81)]
Fitness Average
[('HillClimber', 10757909.18),
 ('RandomSearch', 11509934.05),
 ('ES', 11860599.15),
 ('ILS', 11929806.27)]
```

Here we can see that the HillClimber is the best in fitness and the lowest in "count", this value representing how much the algorithm went into the loop. The HillClimber does not explore much but as we saw it operated much more to get a better fitness. If the fitness was the value that was the most important the RandomSearch would be second, however it has mediocre performance because of how much it spends on the main loop. The ES being the second one in loop iteration and having a good score it would be our real second one. ILS still being lower than Random Search in loop iteration it would be third, as its fitness is very close of the ES one.